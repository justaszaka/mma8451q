/**
 * @file MMA8451Q.c
 * @author Justas Zakarauskas (JustasZaka@gmail.com)
 * @brief MMA8451Q source file
 * @version 0.1
 * @date 2023-11-22
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "stm32f7xx_hal_gpio.h"
#include "stm32f7xx_hal_i2c.h"
#include "stm32f722xx.h"
#include "MMA8451Q.h"

enum{
   MMA8451Q_ERROR_OK,
   MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,
   MMA8451Q_ERROR_WRITE_TO_REG_FAIL,
   MMA8451Q_ERROR_INCORRECT_ADDRESS,
   MMA8451Q_ERROR_NO_COMMUNICATION,
   MMA8451Q_ERROR_READ_TO_REG_FAIL,
   MMA8451Q_ERROR_READ_ANY_AXIS
}eMMA8451QErrorList;


/**
 * @brief MMA8451 IC initialisation.
 * Checks if is possible to communicate to device 
 * @param pObj 
 * @param devAddress 
 * @param pI2cobj 
 */
MMA8451QInit(MMA8451Q_t* pObj, uint8_t devAddress, I2C_HandleTypeDef* pI2cobj)
{
    if((MMA8451Q_ADDRESS_SA0 == devAddress) || (MMA8451Q_ADDRESS_SA1 == devAddress))
    {
        pObj->devAddress = devAddress;
        pObj->errorValue = MMA8451Q_ERROR_OK;
        pObj->i2cHand = pI2cobj;
        
        if(HAL_OK ==  HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,REG_WHO_AM_I,1,pObj->rXData,1,HAL_MAX_DELAY))
        {
            if(WHO_I_AM_RETURN_VALUE != pObj->rXData)
            {
                pObj->errorValue = MMA8451Q_ERROR_NO_COMMUNICATION;
            }
        }
        else
        {
            pObj->errorValue = MMA8451Q_ERROR_WRITE_TO_REG_FAIL;
        }
    }
    else
    {
        memset(pObj,0,sizeof(MMA8451Q_t));
        pObj->errorValue = MMA8451Q_ERROR_INCORRECT_ADDRESS;
    }

}

/**
 * @brief Write to register on MMA8451Q IC.
 * 
 * @param pObj 
 * @param RegName 
 * @param Data 
 */
void  MMA8451QWriteRegister(MMA8451Q_t* pObj, uint8_t RegName, uint8_t Data)
{
    if(((RegName >= 0x07) && (RegName <= 0x08)) || ((RegName >= 0x19) && (RegName <= 0x1C)) || (RegName >= 0x32))
    {
        memset(pObj, 0, sizeof(MMA8451Q_t));
        pObj->errorValue = MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE;
    }

    else
    {
        pObj->regValue = RegName;
        pObj->tXData = Data;
        pObj->errorValue = MMA8451Q_ERROR_OK;
        uint8_t status = HAL_I2C_Mem_Write(pObj->i2cHand,pObj->devAddress,pObj->regValue,1,pObj->tXData,1,HAL_MAX_DELAY);
        if(HAL_OK != status)
        {
            pObj->errorValue = MMA8451Q_ERROR_WRITE_TO_REG_FAIL;
        }
    }
}

/**
 * @brief Read from register on MMA8451Q IC.
 * 
 * @param pObj 
 * @param RegName 
 * @param Data 
 */
void MMA8451QReadRegister(MMA8451Q_t* pObj, uint8_t RegName, uint8_t Data)
{
     if(((RegName >= 0x07) && (RegName <= 0x08)) || ((RegName >= 0x19) && (RegName <= 0x1C)) || (RegName >= 0x32))
    {
        memset(pObj, 0, sizeof(MMA8451Q_t));
        pObj->errorValue = MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE;
    }

    else
    {
        pObj->regValue = RegName;
        pObj->errorValue = MMA8451Q_ERROR_OK;
        uint8_t status = HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,pObj->regValue,1,pObj->rXData,1,HAL_MAX_DELAY);
        if(HAL_OK != status)
        {
            pObj->errorValue = MMA8451Q_ERROR_READ_TO_REG_FAIL;
        }
    }
}

/**
 * @brief Read All Axis (14bit) on MMA8451Q IC.
 * 
 * @param pObj 
 */
void MMA8451QReadAllAxis14bit(MMA8451Q_t* pObj)
{
    uint8_t status = 99;
    
    status = HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_X_MXB),1,&pObj->xAxisMSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_Y_MXB),1,&pObj->yAxisMSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_Z_MXB),1,&pObj->zAxisMSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_X_MXB),1,&pObj->xAxisLSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_X_MXB),1,&pObj->yAxisLSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_X_MXB),1,&pObj->zAxisLSB,1,HAL_MAX_DELAY);
    

    if(HAL_OK == status)
    {

           if ((((pObj->xAxisMSB << 8) | (pObj->xAxisLSB)) <= 0x3FFF && ((pObj->yAxisMSB << 8) | (pObj->yAxisLSB) <= 0x3FFF) && ((pObj->zAxisMSB << 8) | (pObj->zAxisLSB)<= 0x3FFF)))
            {
                pObj->xAxisTotal = (pObj->xAxisMSB << 8) | (pObj->xAxisLSB);
                pObj->yAxisTotal = (pObj->yAxisMSB << 8) | (pObj->yAxisLSB);
                pObj->zAxisTotal = (pObj->zAxisMSB << 8) | (pObj->zAxisLSB);
                pObj->errorValue = HAL_OK;
            }

            else
            {  
                pObj->xAxisMSB = 0; pObj->yAxisMSB = 0;
                pObj->zAxisMSB = 0; pObj->xAxisLSB = 0;
                pObj->yAxisLSB = 0; pObj->zAxisLSB = 0;
                pObj->xAxisTotal = 0;pObj->yAxisTotal = 0; pObj->zAxisTotal = 0;
                pObj->errorValue = MMA8451Q_ERROR_READ_ANY_AXIS;
            }
        
    }
    else if (status != HAL_OK)
    {
            pObj->xAxisMSB = 0; pObj->yAxisMSB = 0;
            pObj->zAxisMSB = 0; pObj->xAxisLSB = 0;
            pObj->yAxisLSB = 0; pObj->zAxisLSB = 0;
            pObj->xAxisTotal = 0;
            pObj->yAxisTotal = 0;
            pObj->zAxisTotal = 0;
        pObj->errorValue = MMA8451Q_ERROR_NO_COMMUNICATION;
    }
}

/**
 * @brief Read All Axis (8bit) on MMA8451Q IC.
 * 
 * @param pObj 
 */
void MMA8451QReadAllAxis8bit(MMA8451Q_t* pObj)
{
    uint8_t status = 99;
    
    status = HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_X_MXB),1,&pObj->xAxisMSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_Y_MXB),1,&pObj->yAxisMSB,1,HAL_MAX_DELAY);
    status += HAL_I2C_Mem_Read(pObj->i2cHand,pObj->devAddress,(REG_OUT_Z_MXB),1,&pObj->zAxisMSB,1,HAL_MAX_DELAY);
    if(HAL_OK == status)
    {
        if ((((pObj->xAxisMSB << 8) | (pObj->xAxisLSB)) <= 0xFF00 && ((pObj->yAxisMSB << 8) | (pObj->yAxisLSB) <= 0xFF00) && ((pObj->zAxisMSB << 8) | (pObj->zAxisLSB)<= 0xFF00)))
        {
            
            pObj->xAxisTotal = pObj->xAxisMSB ;
            pObj->yAxisTotal = pObj->yAxisMSB ;
            pObj->zAxisTotal = pObj->zAxisMSB;
            pObj->errorValue = HAL_OK;
        }
            else
        {
            pObj->xAxisMSB = 0; pObj->yAxisMSB = 0;
            pObj->zAxisMSB = 0; pObj->xAxisLSB = 0;
            pObj->yAxisLSB = 0; pObj->zAxisLSB = 0;
            pObj->xAxisTotal = 0;
            pObj->yAxisTotal = 0;
            pObj->zAxisTotal = 0;
            pObj->errorValue = MMA8451Q_ERROR_READ_ANY_AXIS;
        }
    }
     else if (status != HAL_OK)
    {
            pObj->xAxisMSB = 0; pObj->yAxisMSB = 0;
            pObj->zAxisMSB = 0; pObj->xAxisLSB = 0;
            pObj->yAxisLSB = 0; pObj->zAxisLSB = 0;
            pObj->xAxisTotal = 0;
            pObj->yAxisTotal = 0;
            pObj->zAxisTotal = 0;
        pObj->errorValue = MMA8451Q_ERROR_NO_COMMUNICATION;
    }
}
