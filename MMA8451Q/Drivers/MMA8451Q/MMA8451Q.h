/**
 * @file MMA8451Q.h
 * @author Justas Zakarauskas (JustasZaka@gmail.com)
 * @brief MMA8451Q driver header file
 * @version 0.1
 * @date 2023-11-22
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef MMA8451Q_PUBLIC
#define MMA8451Q_PUBLIC
#include <stdint.h>
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_gpio.h"
#include "stm32f7xx_hal_conf.h"
#include "stm32f7xx_hal_i2c.h"


/* 8 bit masks */
#define BIT_7_MASK (1 << 7)
#define BIT_6_MASK (1 << 6)
#define BIT_5_MASK (1 << 5)
#define BIT_4_MASK (1 << 4)
#define BIT_3_MASK (1 << 3)
#define BIT_2_MASK (1 << 2)
#define BIT_1_MASK (1 << 1)
#define BIT_0_MASK (1 << 0)


#define MAX_COUNTS              8192
#define MMA8451Q_ADDRESS_SA0    0x1C << 1
#define MMA8451Q_ADDRESS_SA1    0x1D << 1 
#define I2C_READ_BIT            1
#define I2C_WRITE_BIT           0

/* Utilities */
#define WHO_I_AM_RETURN_VALUE   0x1A


#define REG_STATUS              0x00
#define REG_OUT_X_MXB           0x01
#define REG_OUT_X_LXB           0x02
#define REG_OUT_Y_MXB           0x03
#define REG_OUT_Y_LXB           0x04
#define REG_OUT_Z_MXB           0x05
#define REG_OUT_Z_LXB           0x06
#define REG_F_SETUP             0x09
#define REG_TRIG_CFG            0x0A
#define REG_SYSMOD              0x0B
#define REG_INT_SOURCE          0x0C
#define REG_WHO_AM_I            0x0D
#define REG_XYZ_CFG             0x0E
#define REG_HP_FILTER_CUTOFF    0x0F
#define REG_PL_STATUS           0x10
#define REG_PL_CFG              0x11
#define REG_PL_COUNT            0x12
#define REG_PL_BF_ZCOMP         0x13
#define REG_PL_THIS_REG         0x14
#define REG_FF_MT_CFG           0x15
#define REG_FF_MT_SRC           0x16
#define REG_FF_MT_THIS          0x17
#define REG_FF_MT_COUNT         0x18
#define REG_TRANCIENT_CFG       0x1D
#define REG_TRANCIENT_SRC       0x1E
#define REG_TRANCIENT_THS       0x1F
#define REG_TRANCIENT_COUNT     0x20
#define REG_PULSE_CFG           0x21
#define REG_PULSE_SRC           0x22
#define REG_PULSE_THSX          0x23
#define REG_PULSE_THSY          0x24
#define REG_PULSE_THSZ          0x25
#define REG_PULSE_TMLT          0x26
#define REG_PULSE_LTCY          0x27
#define REG_PULSE_WIND          0x28
#define REG_ASLP_COUNT          0x29
#define REG_CTRL_REG1           0x2A
#define REG_CTRL_REG2           0x2B
#define REG_CTRL_REG3           0x2C
#define REG_CTRL_REG4           0x2D
#define REG_CTRL_REG5           0x2E
#define REG_OFF_X               0x2F
#define REG_OFF_Y               0x30
#define REG_OFF_Z               0x31

/*  F_MODE = 00: 0x00 STATUS: Data status register (read only) */
#define ZYXOW   BIT_7_MASK
#define ZOW     BIT_6_MASK
#define YOW     BIT_5_MASK
#define XOW     BIT_4_MASK
#define ZYXDR   BIT_3_MASK
#define ZDR     BIT_2_MASK
#define YDR     BIT_1_MASK
#define XDR     BIT_0_MASK

/* F_STATUS: FIFO status register (read only) */
#define F_OVF   BIT_7_MASK
#define F_WMRK_FLAG BIT_6_MASK
#define F_CNT5  BIT_5_MASK
#define F_CNT4  BIT_4_MASK
#define F_CNT3  BIT_3_MASK
#define F_CNT2  BIT_2_MASK
#define F_CNT1  BIT_1_MASK
#define F_CNT0  BIT_0_MASK

/* 9 F_SETUP: FIFO setup register (read/write) */
#define F_MODE1 BIT_7_MASK
#define F_MODE0 BIT_6_MASK
#define F_WMRK5 BIT_5_MASK
#define F_WMRK4 BIT_4_MASK
#define F_WMRK3 BIT_3_MASK
#define F_WMRK2 BIT_2_MASK
#define F_WMRK1 BIT_1_MASK
#define F_WMRK0 BIT_0_MASK

/* TRIG_CFG trigger configuration register (read/write) */
#define TRIG_TRANS  BIT_5_MASK
#define TRIG_LNDPRT BIT_4_MASK
#define TRIG_PULSE  BIT_3_MASK
#define TRIG_FF_MT  BIT_2_MASK

/*  SYSMOD: System mode register (read only) */
#define FGERR       BIT_7_MASK 
#define FGT_4       BIT_6_MASK
#define FGT_3       BIT_5_MASK
#define FGT_2       BIT_4_MASK
#define FGT_1       BIT_3_MASK
#define FGT_0       BIT_2_MASK
#define SYSMOD1     BIT_1_MASK
#define SYSMOD0     BIT_0_MASK

/* INT_SOURCE: system interrupt status register (read only) */
#define SRC_ASLP    BIT_7_MASK
#define SRC_FIFO    BIT_6_MASK
#define SRC_TRANS   BIT_5_MASK
#define SRC_LNDPRT  BIT_4_MASK
#define SRC_PULSE   BIT_3_MASK
#define SRC_FF_MT   BIT_2_MASK
#define SRC_DRDY    BIT_0_MASK
                    
/* WHO_AM_I Device ID register (read only) */
#define HPF_OUT BIT_4_MASK
#define FS1     BIT_1_MASK
#define FS0     BIT_0_MASK

/* HP_FILTER_CUTOFF: high-pass filter register (read/write) */
#define PULSE_HPF_BYP BIT_5_MASK
#define PULSE_LPF_EN BIT_4_MASK
#define SEL1 BIT_1_MASK
#define SEL0 BIT_0_MASK

/* PL_STATUS register (read only) */
#define NEWLP BIT_7_MASK
#define LO    BIT_6_MASK
#define LAPO1 BIT_2_MASK
#define LAPO0 BIT_1_MASK
#define BAFR0 BIT_0_MASK

/*  PL_CFG register (read/write) */
#define CFG_DBCNTM  BIT_7_MASK
#define PL_EN       BIT_6_MASK

/* PL_COUNT register (read/write) */
#define DBNCE7 BIT_7_MASK
#define DBNCE6 BIT_6_MASK
#define DBNCE5 BIT_5_MASK
#define DBNCE4 BIT_4_MASK
#define DBNCE3 BIT_3_MASK
#define DBNCE2 BIT_2_MASK
#define DBNCE1 BIT_1_MASK
#define DBNCE0 BIT_0_MASK
 
/*  PL_BF_ZCOMP register (read/write) */
#define BKFR1 BIT_7_MASK
#define BKFR0 BIT_6_MASK
#define ZCLOCK2 BIT_2_MASK
#define ZCLOCK1 BIT_1_MASK
#define ZCLOCK0 BIT_0_MASK

/* PL_THS_REG register (read/write) */
#define PL_THS4 BIT_7_MASK
#define PL_THS3 BIT_6_MASK
#define PL_THS2 BIT_5_MASK
#define PL_THS1 BIT_4_MASK
#define PL_THS0 BIT_3_MASK
#define HYS2    BIT_2_MASK
#define HYS1    BIT_1_MASK
#define HYS0    BIT_0_MASK

/* FF_MT_CFG register (read/write) */
#define FF_CFG_ELE  BIT_7_MASK  
#define OAE  BIT_6_MASK
#define ZEFE BIT_5_MASK 
#define YEFE BIT_4_MASK 
#define XEFE BIT_3_MASK 

/* FF_MT_SRC freefall and motion source register (read only) */
#define EA  BIT_7_MASK
#define ZHE BIT_5_MASK
#define ZHP BIT_4_MASK
#define YHE BIT_3_MASK
#define YHP BIT_2_MASK
#define XHE BIT_1_MASK
#define XHP BIT_0_MASK

/*  FF_MT_THS register (read/write)  */
#define DBCNTM BIT_7_MASK   
#define THS6   BIT_6_MASK
#define THS5   BIT_5_MASK
#define THS4   BIT_4_MASK
#define THS3   BIT_3_MASK
#define THS2   BIT_2_MASK
#define THS1   BIT_1_MASK
#define THS0   BIT_0_MASK

// /* FF_MT_COUNT register (read/write) */
#define FFT_MT_D7 BIT_7_MASK 
#define FFT_MT_D6 BIT_6_MASK
#define FFT_MT_D5 BIT_5_MASK
#define FFT_MT_D4 BIT_4_MASK
#define FFT_MT_D3 BIT_3_MASK
#define FFT_MT_D2 BIT_2_MASK
#define FFT_MT_D1 BIT_1_MASK
#define FFT_MT_D0 BIT_0_MASK

/* TRANSIENT_CFG register (read/write) */
#define ELE     BIT_4_MASK
#define ZTEFE   BIT_3_MASK
#define YTEFE   BIT_2_MASK
#define XTEFE   BIT_1_MASK
#define HPF_BYP BIT_0_MASK

/* TRANSIENT_SRC register (read only)  */
#define TRANSEIENT_D7   BIT_7_MASK
#define TRANSEIENT_D6   BIT_6_MASK
#define TRANSEIENT_D5   BIT_5_MASK
#define TRANSEIENT_D4   BIT_4_MASK
#define TRANSEIENT_D3   BIT_3_MASK
#define TRANSEIENT_D2   BIT_2_MASK
#define TRANSEIENT_D1   BIT_1_MASK
#define TRANSEIENT_D0   BIT_0_MASK

/*  TRANSIENT_THS register (read/write) */
#define THS_DBCNTM  BIT_7_MASK
#define THS6        BIT_6_MASK
#define THS5        BIT_5_MASK
#define THS4        BIT_4_MASK
#define THS3        BIT_3_MASK
#define THS2        BIT_2_MASK
#define THS1        BIT_1_MASK
#define THS0        BIT_0_MASK

/* TRANSIENT_COUNT register (read/write) */
#define TCD7        BIT_7_MASK
#define TCD6        BIT_6_MASK
#define TCD5        BIT_5_MASK
#define TCD4        BIT_4_MASK
#define TCD3        BIT_3_MASK
#define TCD2        BIT_2_MASK
#define TCD1        BIT_1_MASK
#define TCD0        BIT_0_MASK

/* PULSE_CFG register (read/write) */
#define DPA             BIT_7_MASK
#define PULSE_CFG_ELE   BIT_6_MASK
#define ZDPEFE          BIT_5_MASK
#define ZSPEFE          BIT_4_MASK
#define YDPEFE          BIT_3_MASK
#define YSPEFE          BIT_2_MASK
#define XDPEFE          BIT_1_MASK
#define XSPEFE          BIT_0_MASK


/* PULSE_SRC register (read only) */
#define EA      BIT_7_MASK
#define AXZ     BIT_6_MASK
#define AXY     BIT_5_MASK
#define AXX     BIT_4_MASK
#define DPE     BIT_3_MASK
#define POIZ    BIT_2_MASK
#define POIY    BIT_1_MASK
#define POIX    BIT_0_MASK

/* PULSE_THSX register (read/write) */
#define THSX6  BIT_6_MASK
#define THSX5  BIT_5_MASK
#define THSX4  BIT_4_MASK
#define THSX3  BIT_3_MASK
#define THSX2  BIT_2_MASK
#define THSX1  BIT_1_MASK
#define THSX0  BIT_0_MASK

/* PULSE_THSY register (read/write) */
#define THSY6 BIT_6_MASK
#define THSY5 BIT_5_MASK
#define THSY4 BIT_4_MASK
#define THSY3 BIT_3_MASK
#define THSY2 BIT_2_MASK
#define THSY1 BIT_1_MASK
#define THSY0 BIT_0_MASK

/* PULSE_THSZ register (read/write) */
#define THSZ6 BIT_6_MASK
#define THSZ5 BIT_5_MASK
#define THSZ4 BIT_4_MASK
#define THSZ3 BIT_3_MASK
#define THSZ2 BIT_2_MASK
#define THSZ1 BIT_1_MASK
#define THSZ0 BIT_0_MASK

/* PULSE_TMLT register (read/write) */
#define TMLT7 BIT_7_MASK 
#define TMLT6 BIT_6_MASK
#define TMLT5 BIT_5_MASK
#define TMLT4 BIT_4_MASK
#define TMLT3 BIT_3_MASK
#define TMLT2 BIT_2_MASK
#define TMLT1 BIT_1_MASK
#define TMLT0 BIT_0_MASK

/* PULSE_LTCY register (read/write) */
#define LTCY7 BIT_7_MASK
#define LTCY6 BIT_6_MASK
#define LTCY5 BIT_5_MASK
#define LTCY4 BIT_4_MASK
#define LTCY3 BIT_3_MASK
#define LTCY2 BIT_2_MASK
#define LTCY1 BIT_1_MASK
#define LTCY0 BIT_0_MASK

/* PULSE_WIND register (read/write) */
#define WIND7 BIT_7_MASK
#define WIND6 BIT_6_MASK
#define WIND5 BIT_5_MASK
#define WIND4 BIT_4_MASK
#define WIND3 BIT_3_MASK
#define WIND2 BIT_2_MASK
#define WIND1 BIT_1_MASK
#define WIND0 BIT_0_MASK

/* ASLP_COUNT register (read/write) */
#define ASTCD7 BIT_7_MASK
#define ASTCD6 BIT_6_MASK
#define ASTCD5 BIT_5_MASK
#define ASTCD4 BIT_4_MASK
#define ASTCD3 BIT_3_MASK
#define ASTCD2 BIT_2_MASK
#define ASTCD1 BIT_1_MASK
#define ASTCD0 BIT_0_MASK

/* CTRL_REG1 register (read/write) */
#define ASLP_RATE1  BIT_7_MASK
#define ASLP_RATE0  BIT_6_MASK
#define CRDR2       BIT_5_MASK
#define CRDR1       BIT_4_MASK
#define CRDR0       BIT_3_MASK
#define LNOISE      BIT_2_MASK
#define F_READ      BIT_1_MASK
#define ACTIVE      BIT_0_MASK

/* CTRL_REG2 register (read/write) */
#define ST      BIT_7_MASK
#define RST     BIT_6_MASK
#define SMODS1  BIT_4_MASK
#define SMODS0  BIT_3_MASK
#define SLPE    BIT_2_MASK
#define MODS1   BIT_1_MASK
#define MODS0   BIT_0_MASK

/* CTRL_REG3 register (read/write) */
#define FIFO_GATE       BIT_7_MASK   
#define WAKE_TRANS      BIT_6_MASK
#define WAKE_LNDPRT     BIT_5_MASK
#define WAKE_PULSE      BIT_4_MASK
#define WAKE_FF_MT      BIT_3_MASK
#define IPOL            BIT_1_MASK
#define PP_OD           BIT_0_MASK

/* CTRL_REG4 register (read/write) */
#define INT_EN_ASLP    BIT_7_MASK
#define INT_EN_FIFO    BIT_6_MASK
#define INT_EN_TRANS   BIT_5_MASK
#define INT_EN_LNDPRT  BIT_4_MASK
#define INT_EN_PULSE   BIT_3_MASK
#define INT_EN_FF_MT   BIT_2_MASK
#define INT_EN_DRDY    BIT_0_MASK

/* CTRL_REG4 register (read/write) */
#define INT_CFG_ASLP    BIT_7_MASK  
#define INT_CFG_FIFO    BIT_6_MASK
#define INT_CFG_TRANS   BIT_5_MASK
#define INT_CFG_LNDPRT  BIT_4_MASK 
#define INT_CFG_PULSE   BIT_3_MASK
#define INT_CFG_FF_MT   BIT_2_MASK
#define INT_CFG_DRDY    BIT_0_MASK

typedef struct 
{   
    I2C_HandleTypeDef* i2cHand;
    uint8_t devAddress;
    uint8_t tXData;
    uint8_t rXData;
    uint8_t regValue;
    uint8_t errorValue;
    uint8_t xAxisMSB;
    uint8_t xAxisLSB;
    uint8_t yAxisMSB;
    uint8_t yAxisLSB;
    uint8_t zAxisMSB;
    uint8_t zAxisLSB;
    uint8_t fastMode;
    uint16_t xAxisTotal ;
    uint16_t yAxisTotal ; 
    uint16_t zAxisTotal ;

    
}MMA8451Q_t;




int foo(uint8_t* input, uint8_t mew);
void bar(uint8_t* input, uint8_t number);


#endif
