#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal_i2c_ex.h"
#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal_def.h"
#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal_dma.h"
typedef struct

{

  uint32_t Timing;







  uint32_t OwnAddress1;





  uint32_t AddressingMode;





  uint32_t DualAddressMode;





  uint32_t OwnAddress2;





  uint32_t OwnAddress2Masks;







  uint32_t GeneralCallMode;





  uint32_t NoStretchMode;





} I2C_InitTypeDef;

typedef enum

{

  HAL_I2C_STATE_RESET = 0x00U,

  HAL_I2C_STATE_READY = 0x20U,

  HAL_I2C_STATE_BUSY = 0x24U,

  HAL_I2C_STATE_BUSY_TX = 0x21U,

  HAL_I2C_STATE_BUSY_RX = 0x22U,

  HAL_I2C_STATE_LISTEN = 0x28U,

  HAL_I2C_STATE_BUSY_TX_LISTEN = 0x29U,



  HAL_I2C_STATE_BUSY_RX_LISTEN = 0x2AU,



  HAL_I2C_STATE_ABORT = 0x60U,

  HAL_I2C_STATE_TIMEOUT = 0xA0U,

  HAL_I2C_STATE_ERROR = 0xE0U



} HAL_I2C_StateTypeDef;

typedef enum

{

  HAL_I2C_MODE_NONE = 0x00U,

  HAL_I2C_MODE_MASTER = 0x10U,

  HAL_I2C_MODE_SLAVE = 0x20U,

  HAL_I2C_MODE_MEM = 0x40U



} HAL_I2C_ModeTypeDef;

typedef struct __I2C_HandleTypeDef

{

  I2C_TypeDef *Instance;



  I2C_InitTypeDef Init;



  uint8_t *pBuffPtr;



  uint16_t XferSize;



  volatile uint16_t XferCount;



  volatile uint32_t XferOptions;





  volatile uint32_t PreviousState;



  HAL_StatusTypeDef(*XferISR)(struct __I2C_HandleTypeDef *hi2c, uint32_t ITFlags, uint32_t ITSources);





  DMA_HandleTypeDef *hdmatx;



  DMA_HandleTypeDef *hdmarx;



  HAL_LockTypeDef Lock;



  volatile HAL_I2C_StateTypeDef State;



  volatile HAL_I2C_ModeTypeDef Mode;



  volatile uint32_t ErrorCode;



  volatile uint32_t AddrEventCount;



  volatile uint32_t Devaddress;



  volatile uint32_t Memaddress;

} I2C_HandleTypeDef;

HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c);

HAL_StatusTypeDef HAL_I2C_DeInit(I2C_HandleTypeDef *hi2c);

void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c);

void HAL_I2C_MspDeInit(I2C_HandleTypeDef *hi2c);

HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                          uint16_t Size, uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                         uint16_t Size, uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_Slave_Transmit(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size,

                                         uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_Slave_Receive(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size,

                                        uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress,

                                    uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_Mem_Read(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress,

                                   uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_IsDeviceReady(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint32_t Trials,

                                        uint32_t Timeout);





HAL_StatusTypeDef HAL_I2C_Master_Transmit_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                             uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Master_Receive_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                            uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Slave_Transmit_IT(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Slave_Receive_IT(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Mem_Write_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress,

                                       uint16_t MemAddSize, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Mem_Read_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress,

                                      uint16_t MemAddSize, uint8_t *pData, uint16_t Size);



HAL_StatusTypeDef HAL_I2C_Master_Seq_Transmit_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                                 uint16_t Size, uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_Master_Seq_Receive_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                                uint16_t Size, uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_Slave_Seq_Transmit_IT(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size,

                                                uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_Slave_Seq_Receive_IT(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size,

                                               uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_EnableListen_IT(I2C_HandleTypeDef *hi2c);

HAL_StatusTypeDef HAL_I2C_DisableListen_IT(I2C_HandleTypeDef *hi2c);

HAL_StatusTypeDef HAL_I2C_Master_Abort_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress);





HAL_StatusTypeDef HAL_I2C_Master_Transmit_DMA(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                              uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Master_Receive_DMA(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                             uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Slave_Transmit_DMA(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Slave_Receive_DMA(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Mem_Write_DMA(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress,

                                        uint16_t MemAddSize, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef HAL_I2C_Mem_Read_DMA(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress,

                                       uint16_t MemAddSize, uint8_t *pData, uint16_t Size);



HAL_StatusTypeDef HAL_I2C_Master_Seq_Transmit_DMA(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                                  uint16_t Size, uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_Master_Seq_Receive_DMA(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,

                                                 uint16_t Size, uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_Slave_Seq_Transmit_DMA(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size,

                                                 uint32_t XferOptions);

HAL_StatusTypeDef HAL_I2C_Slave_Seq_Receive_DMA(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size,

                                                uint32_t XferOptions);

void HAL_I2C_EV_IRQHandler(I2C_HandleTypeDef *hi2c);

void HAL_I2C_ER_IRQHandler(I2C_HandleTypeDef *hi2c);

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t TransferDirection, uint16_t AddrMatchCode);

void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c);

void HAL_I2C_AbortCpltCallback(I2C_HandleTypeDef *hi2c);

HAL_I2C_StateTypeDef HAL_I2C_GetState(I2C_HandleTypeDef *hi2c);

HAL_I2C_ModeTypeDef HAL_I2C_GetMode(I2C_HandleTypeDef *hi2c);

uint32_t HAL_I2C_GetError(I2C_HandleTypeDef *hi2c);
