#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal_i2c.h"
#include "../../Core/Inc/stm32f7xx_hal_conf.h"
#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal_gpio.h"
#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal.h"
typedef struct

{

    I2C_HandleTypeDef* i2cHand;

    uint8_t devAddress;

    uint8_t tXData;

    uint8_t rXData;

    uint8_t regValue;

    uint8_t errorValue;

}MMA8451Q_t;
