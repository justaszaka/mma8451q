#include "../../Drivers/MMA8451Q/MMA8451Q.c"
#include "build/test/mocks/mock_stm32f7xx_hal_i2c.h"
#include "build/test/mocks/mock_stm32f7xx_hal_gpio.h"
#include "../../Drivers/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal_dma.h"
#include "../../Drivers/CMSIS/Device/ST/STM32F7xx/Include/stm32f7xx.h"
#include "C:/Ruby27-x64/lib/ruby/gems/2.7.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"










void test_MMA8451Q_PRIVATE_Error_Enum_List(void)

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((0)), (

   ((void *)0)

   ), (UNITY_UINT)(21), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((1)), (

   ((void *)0)

   ), (UNITY_UINT)(22), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_WRITE_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((2)), (

   ((void *)0)

   ), (UNITY_UINT)(23), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_INCORRECT_ADDRESS)), (UNITY_INT)(UNITY_UINT8 )((3)), (

   ((void *)0)

   ), (UNITY_UINT)(24), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_NO_COMMUNICATION)), (UNITY_INT)(UNITY_UINT8 )((4)), (

   ((void *)0)

   ), (UNITY_UINT)(25), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((5)), (

   ((void *)0)

   ), (UNITY_UINT)(26), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_ANY_AXIS)), (UNITY_INT)(UNITY_UINT8 )((6)), (

   ((void *)0)

   ), (UNITY_UINT)(27), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PRIVATE_MMA8451Q_SendData_Incorrect_Register_value(void)

{

    uint8_t DataExample = 0xAA;

    MMA8451Q_t obj;

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_ERROR);

    MMA8451QWriteRegister(&obj,0x07,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(37), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(38), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(39), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x08,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(42), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(43), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(44), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x19,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(47), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(48), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(49), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x1A,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(52), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(53), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(54), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x1B,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(57), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(58), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(59), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x1C,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(62), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(63), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(64), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x32,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(67), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(68), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(69), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0xFF,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(72), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(73), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(74), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_MMA8451Q_WriteToRegister_Correct_Register_value(void)

{

    MMA8451Q_t obj;

    uint8_t DataExample = 0xAA;

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_OK);

    MMA8451QWriteRegister(&obj,0x00,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(83), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(84), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((DataExample)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(85), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x06 ,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (

   ((void *)0)

   ), (UNITY_UINT)(88), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x06)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(89), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((DataExample)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(90), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x09,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (

   ((void *)0)

   ), (UNITY_UINT)(93), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x09)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(94), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((DataExample)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(95), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x18,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (

   ((void *)0)

   ), (UNITY_UINT)(98), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x18)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(99), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((DataExample)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(100), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x1D,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(103), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x1D)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(104), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((DataExample)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(105), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QWriteRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(108), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x31)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(109), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((DataExample)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(110), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_MMA8451Q_WriteToReg_Success(void)

{

    MMA8451Q_t obj;

    uint8_t DataExample = 0xAA;

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_OK);

    MMA8451QWriteRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(119), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((1)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(120), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_MMA8451Q_WriteToReg_FAIL(void)

{

    MMA8451Q_t obj;

    uint8_t DataExample = 0xAA;

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_ERROR);

    MMA8451QWriteRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_WRITE_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(129), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((HAL_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(130), UNITY_DISPLAY_STYLE_UINT8);



    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_BUSY);

    MMA8451QWriteRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_WRITE_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(134), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((HAL_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(135), UNITY_DISPLAY_STYLE_UINT8);



    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0,HAL_TIMEOUT);

    MMA8451QWriteRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_WRITE_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(139), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((HAL_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(140), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_INIT_INCORRECT_ADDRESS(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;



    MMA8451QInit(&obj,0XFF,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_INCORRECT_ADDRESS)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(149), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.devAddress)), (

   ((void *)0)

   ), (UNITY_UINT)(150), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QInit(&obj,0xAA,&i2cobj);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(153), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xAA)), (UNITY_INT)(UNITY_UINT8 )((obj.devAddress)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(154), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_INIT_CORRECT_ADDRESS(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;



    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    obj.rXData = 0x1A;

    MMA8451QInit(&obj,0x1C << 1,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(165), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x1C << 1)), (UNITY_INT)(UNITY_UINT8 )((obj.devAddress)), (

   ((void *)0)

   ), (UNITY_UINT)(166), UNITY_DISPLAY_STYLE_UINT8);



    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    obj.rXData = 0x1A;

    MMA8451QInit(&obj,0x1D << 1,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(171), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x1D << 1)), (UNITY_INT)(UNITY_UINT8 )((obj.devAddress)), (

   ((void *)0)

   ), (UNITY_UINT)(172), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PRIVATE_INIT_INCOCORRECT_I2C_HANDLER(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;

    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    MMA8451QInit(&obj,0x1C << 1,&i2cobj);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT32)((0)), (UNITY_INT)(UNITY_UINT32)((obj.i2cHand)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(182), UNITY_DISPLAY_STYLE_UINT32);

}



void test_MMA8451Q_PRIVATE_INIT_CORRECT_I2C_HANDLER(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;

    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    MMA8451QInit(&obj,0x1C << 1,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT32)((&i2cobj)), (UNITY_INT)(UNITY_UINT32)((obj.i2cHand)), (

   ((void *)0)

   ), (UNITY_UINT)(191), UNITY_DISPLAY_STYLE_UINT32);

}



void test_MMA8451Q_PRIVATE_INIT_FAILED_COMMUNICATION(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;

    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    MMA8451QInit(&obj,0x1C << 1,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_NO_COMMUNICATION)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(200), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_INIT_FAIL_COMMUNICATION(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;

    obj.rXData = 0xAA;

    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    MMA8451QInit(&obj,0x1C << 1,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_NO_COMMUNICATION)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(210), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PRIVATE_INIT_SUCCESS_COMMUNICATION(void)

{

    MMA8451Q_t obj;

    I2C_HandleTypeDef i2cobj;

    obj.rXData = 0x1A;

    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);

    MMA8451QInit(&obj,0x1C << 1,&i2cobj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(221), UNITY_DISPLAY_STYLE_UINT8);

}









void test_MMA8451Q_PRIVATE_MMA8451Q_ReadData_Incorrect_Register_value(void)

{

    uint8_t DataExample = 0xAA;

    MMA8451Q_t obj;

    obj.errorValue = HAL_OK ;

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_ERROR);

    MMA8451QReadRegister(&obj,0x07,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(234), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(235), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(236), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x08,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(239), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(240), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(241), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x19,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(244), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(245), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(246), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x1A,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(249), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(250), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(251), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x1B,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(254), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(255), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(256), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x1C,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(259), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(260), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(261), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x32,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(264), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(265), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(266), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0xFF,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(269), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(270), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.tXData)), (

   ((void *)0)

   ), (UNITY_UINT)(271), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_MMA8451Q_ReadToRegister_Correct_Register_value(void)

{

    MMA8451Q_t obj;

    uint8_t DataExample = 0xAA;

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_OK);

    MMA8451QReadRegister(&obj,0x00,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(280), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(281), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x06 ,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (

   ((void *)0)

   ), (UNITY_UINT)(284), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x06)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(285), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x09,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (

   ((void *)0)

   ), (UNITY_UINT)(288), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x09)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(289), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x18,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (

   ((void *)0)

   ), (UNITY_UINT)(292), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x18)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(293), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x1D,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(296), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x1D)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(297), UNITY_DISPLAY_STYLE_UINT8);



    MMA8451QReadRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(300), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x31)), (UNITY_INT)(UNITY_UINT8 )((obj.regValue)), (

   ((void *)0)

   ), (UNITY_UINT)(301), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_MMA8451Q_ReadFromReg_Success(void)

{

    MMA8451Q_t obj;

    uint8_t DataExample = 0xAA;

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_OK);

    MMA8451QReadRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(310), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((1)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(311), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_MMA8451Q_ReadFromReg_FAIL(void)

{

    MMA8451Q_t obj;

    uint8_t DataExample = 0xAA;

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_ERROR);

    MMA8451QReadRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(320), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((HAL_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(321), UNITY_DISPLAY_STYLE_UINT8);



    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_BUSY);

    MMA8451QReadRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(325), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((HAL_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(326), UNITY_DISPLAY_STYLE_UINT8);



    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0,HAL_TIMEOUT);

    MMA8451QReadRegister(&obj,0x31,DataExample);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_TO_REG_FAIL)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(330), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT)(UNITY_UINT8 )((HAL_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), UNITY_NOT_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(331), UNITY_DISPLAY_STYLE_UINT8);

}

void test_MMA8451Q_PRIVATE_READ_ALL_AXIS_14BIT_FAIL_COMMONUCATION()

{

    MMA8451Q_t obj;

    uint8_t ExpectedData = 29;

    for (int i = 0; i < 6 ; i++)

    {

        HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_ERROR);

    }

    MMA8451QReadAllAxis14bit(&obj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_NO_COMMUNICATION)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(371), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_SUCCESS_COMMUNICATION()

{

    MMA8451Q_t obj = {0};

    uint8_t ExpectedData = 29;

    for (int i = 0; i < 6 ; i++)

    {

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_OK);

    }



    MMA8451QReadAllAxis14bit(&obj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(384), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_INCORRECT_READING_14_BIT()

{

    MMA8451Q_t obj;

    obj.fastMode = 0;

    obj.xAxisMSB = 0xFF;

    obj.xAxisLSB = 0xFF;

    obj.yAxisMSB = 0x3F;

    obj.yAxisLSB = 0xFF;

    obj.zAxisMSB = 0x3F;

    obj.zAxisLSB = 0xFF;



    for (int i = 0; i < 6 ; i++)

    {

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_OK);

    }

    MMA8451QReadAllAxis14bit(&obj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_ANY_AXIS)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(403), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(404), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(405), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(406), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(407), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(408), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(409), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(410), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(411), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(412), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_CORRECT_READING_14_BIT()

{

    MMA8451Q_t obj;

    obj.fastMode = 0;

    obj.xAxisMSB = 0x3A;

    obj.xAxisLSB = 0xFF;

    obj.yAxisMSB = 0x3B;

    obj.yAxisLSB = 0xFF;

    obj.zAxisMSB = 0x3C;

    obj.zAxisLSB = 0xFF;

    for (int i = 0; i < 6 ; i++)

    {

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(456,HAL_OK);

    }

    MMA8451QReadAllAxis14bit(&obj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(431), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x3A)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(432), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xFF)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(433), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x3B)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(434), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xFF)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(435), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x3C)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(436), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xFF)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(437), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x3AFF)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(438), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x3BFF)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(439), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x3CFF)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(440), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_INCORRECT_READING_8_BIT()

{

    MMA8451Q_t obj;

    obj.fastMode = 1;

    obj.xAxisMSB = 0xFF;

    obj.xAxisLSB = 1;

    obj.yAxisMSB = 0xFF;

    obj.xAxisLSB = 1;

    obj.zAxisMSB = 0xFF;

    obj.xAxisLSB = 1;





    for (int i = 0; i < 3 ; i++)

    {

        HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_OK);

    }

    MMA8451QReadAllAxis8bit(&obj);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(461), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(462), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(463), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(464), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(465), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(466), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_READ_ANY_AXIS)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(467), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_CORRECT_READING_8_BIT()

{

    MMA8451Q_t obj;

    obj.fastMode = 1;

    obj.xAxisMSB = 0xFF;

    obj.xAxisLSB = 0x00;

    obj.yAxisMSB = 0xFF;

    obj.yAxisLSB = 0x00;

    obj.zAxisMSB = 0xFF;

    obj.zAxisLSB = 0x00;

    for (int i = 0; i < 3 ; i++)

    {

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(456,HAL_OK);

    }

    MMA8451QReadAllAxis8bit(&obj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_OK)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(485), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xFF)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(486), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(487), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xFF)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(488), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(489), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0xFF)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(490), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x00)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(491), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_FAIL_COMMUNICATING_READING_8_BIT()

{

    MMA8451Q_t obj;

    obj.fastMode = 0;

    obj.xAxisMSB = 0x00;

    obj.xAxisLSB = 0x00;

    obj.yAxisMSB = 0x00;

    obj.yAxisLSB = 0x00;

    obj.zAxisMSB = 0x00;

    obj.zAxisLSB = 0x00;



    for (int i = 0; i < 6 ; i++)

    {

        HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_ERROR);

    }

    MMA8451QReadAllAxis8bit(&obj);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((MMA8451Q_ERROR_NO_COMMUNICATION)), (UNITY_INT)(UNITY_UINT8 )((obj.errorValue)), (

   ((void *)0)

   ), (UNITY_UINT)(510), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(511), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(512), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(513), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(514), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisMSB)), (

   ((void *)0)

   ), (UNITY_UINT)(515), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisLSB)), (

   ((void *)0)

   ), (UNITY_UINT)(516), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.xAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(517), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.zAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(518), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((obj.yAxisTotal)), (

   ((void *)0)

   ), (UNITY_UINT)(519), UNITY_DISPLAY_STYLE_UINT8);

}
