#include "build/test/mocks/mock_stm32f7xx_hal_i2c.h"
#include "build/test/mocks/mock_stm32f7xx_hal_gpio.h"
#include "C:/Ruby27-x64/lib/ruby/gems/2.7.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"
#include "../../Drivers/MMA8451Q/MMA8451Q.h"










void setUp(void)

{

}



void tearDown(void)

{

}



void test_MMA8451Q_PUBLIC_Parameters(void)

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((8192)), (UNITY_INT)(UNITY_UINT16)((8192)), (

   ((void *)0)

   ), (UNITY_UINT)(21), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x1C << 1)), (UNITY_INT)(UNITY_UINT16)((0x1C << 1)), (

   ((void *)0)

   ), (UNITY_UINT)(22), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x1D << 1)), (UNITY_INT)(UNITY_UINT16)((0x1D << 1)), (

   ((void *)0)

   ), (UNITY_UINT)(23), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((1)), (UNITY_INT)(UNITY_UINT16)((0x01)), (

   ((void *)0)

   ), (UNITY_UINT)(24), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0)), (UNITY_INT)(UNITY_UINT16)((0x00)), (

   ((void *)0)

   ), (UNITY_UINT)(25), UNITY_DISPLAY_STYLE_UINT16);

}



void test_MMA8451Q_PUBLIC_Utilities(void)

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0x1A)), (UNITY_INT)(UNITY_UINT8 )((0X1A)), (

   ((void *)0)

   ), (UNITY_UINT)(30), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(31), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(32), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(33), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(34), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(35), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(36), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(37), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(38), UNITY_DISPLAY_STYLE_UINT8);

}







void test_MMA8451Q_PUBLIC_Registers(void)

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x00)), (UNITY_INT)(UNITY_UINT16)((0x00)), (

   ((void *)0)

   ), (UNITY_UINT)(45), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x01)), (UNITY_INT)(UNITY_UINT16)((0x01)), (

   ((void *)0)

   ), (UNITY_UINT)(46), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x02)), (UNITY_INT)(UNITY_UINT16)((0x02)), (

   ((void *)0)

   ), (UNITY_UINT)(47), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x03)), (UNITY_INT)(UNITY_UINT16)((0x03)), (

   ((void *)0)

   ), (UNITY_UINT)(48), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x04)), (UNITY_INT)(UNITY_UINT16)((0x04)), (

   ((void *)0)

   ), (UNITY_UINT)(49), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x05)), (UNITY_INT)(UNITY_UINT16)((0x05)), (

   ((void *)0)

   ), (UNITY_UINT)(50), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x06)), (UNITY_INT)(UNITY_UINT16)((0x06)), (

   ((void *)0)

   ), (UNITY_UINT)(51), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x09)), (UNITY_INT)(UNITY_UINT16)((0x09)), (

   ((void *)0)

   ), (UNITY_UINT)(52), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x0A)), (UNITY_INT)(UNITY_UINT16)((0x0A)), (

   ((void *)0)

   ), (UNITY_UINT)(53), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x0B)), (UNITY_INT)(UNITY_UINT16)((0x0B)), (

   ((void *)0)

   ), (UNITY_UINT)(54), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x0C)), (UNITY_INT)(UNITY_UINT16)((0x0C)), (

   ((void *)0)

   ), (UNITY_UINT)(55), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x0D)), (UNITY_INT)(UNITY_UINT16)((0x0D)), (

   ((void *)0)

   ), (UNITY_UINT)(56), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x0E)), (UNITY_INT)(UNITY_UINT16)((0x0E)), (

   ((void *)0)

   ), (UNITY_UINT)(57), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x0F)), (UNITY_INT)(UNITY_UINT16)((0x0F)), (

   ((void *)0)

   ), (UNITY_UINT)(58), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x10)), (UNITY_INT)(UNITY_UINT16)((0x10)), (

   ((void *)0)

   ), (UNITY_UINT)(59), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x11)), (UNITY_INT)(UNITY_UINT16)((0x11)), (

   ((void *)0)

   ), (UNITY_UINT)(60), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x12)), (UNITY_INT)(UNITY_UINT16)((0x12)), (

   ((void *)0)

   ), (UNITY_UINT)(61), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x13)), (UNITY_INT)(UNITY_UINT16)((0x13)), (

   ((void *)0)

   ), (UNITY_UINT)(62), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x14)), (UNITY_INT)(UNITY_UINT16)((0x14)), (

   ((void *)0)

   ), (UNITY_UINT)(63), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x15)), (UNITY_INT)(UNITY_UINT16)((0x15)), (

   ((void *)0)

   ), (UNITY_UINT)(64), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x16)), (UNITY_INT)(UNITY_UINT16)((0x16)), (

   ((void *)0)

   ), (UNITY_UINT)(65), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x17)), (UNITY_INT)(UNITY_UINT16)((0x17)), (

   ((void *)0)

   ), (UNITY_UINT)(66), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x18)), (UNITY_INT)(UNITY_UINT16)((0x18)), (

   ((void *)0)

   ), (UNITY_UINT)(67), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x1D)), (UNITY_INT)(UNITY_UINT16)((0x1D)), (

   ((void *)0)

   ), (UNITY_UINT)(68), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x1E)), (UNITY_INT)(UNITY_UINT16)((0x1E)), (

   ((void *)0)

   ), (UNITY_UINT)(69), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x1F)), (UNITY_INT)(UNITY_UINT16)((0x1F)), (

   ((void *)0)

   ), (UNITY_UINT)(70), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x20)), (UNITY_INT)(UNITY_UINT16)((0x20)), (

   ((void *)0)

   ), (UNITY_UINT)(71), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x21)), (UNITY_INT)(UNITY_UINT16)((0x21)), (

   ((void *)0)

   ), (UNITY_UINT)(72), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x22)), (UNITY_INT)(UNITY_UINT16)((0x22)), (

   ((void *)0)

   ), (UNITY_UINT)(73), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x23)), (UNITY_INT)(UNITY_UINT16)((0x23)), (

   ((void *)0)

   ), (UNITY_UINT)(74), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x24)), (UNITY_INT)(UNITY_UINT16)((0x24)), (

   ((void *)0)

   ), (UNITY_UINT)(75), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x25)), (UNITY_INT)(UNITY_UINT16)((0x25)), (

   ((void *)0)

   ), (UNITY_UINT)(76), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x26)), (UNITY_INT)(UNITY_UINT16)((0x26)), (

   ((void *)0)

   ), (UNITY_UINT)(77), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x27)), (UNITY_INT)(UNITY_UINT16)((0x27)), (

   ((void *)0)

   ), (UNITY_UINT)(78), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x28)), (UNITY_INT)(UNITY_UINT16)((0x28)), (

   ((void *)0)

   ), (UNITY_UINT)(79), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x29)), (UNITY_INT)(UNITY_UINT16)((0x29)), (

   ((void *)0)

   ), (UNITY_UINT)(80), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x2A)), (UNITY_INT)(UNITY_UINT16)((0x2A)), (

   ((void *)0)

   ), (UNITY_UINT)(81), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x2B)), (UNITY_INT)(UNITY_UINT16)((0x2B)), (

   ((void *)0)

   ), (UNITY_UINT)(82), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x2C)), (UNITY_INT)(UNITY_UINT16)((0x2C)), (

   ((void *)0)

   ), (UNITY_UINT)(83), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x2D)), (UNITY_INT)(UNITY_UINT16)((0x2D)), (

   ((void *)0)

   ), (UNITY_UINT)(84), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x2E)), (UNITY_INT)(UNITY_UINT16)((0x2E)), (

   ((void *)0)

   ), (UNITY_UINT)(85), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x2F)), (UNITY_INT)(UNITY_UINT16)((0x2F)), (

   ((void *)0)

   ), (UNITY_UINT)(86), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x30)), (UNITY_INT)(UNITY_UINT16)((0x30)), (

   ((void *)0)

   ), (UNITY_UINT)(87), UNITY_DISPLAY_STYLE_UINT16);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT16)((0x31)), (UNITY_INT)(UNITY_UINT16)((0x31)), (

   ((void *)0)

   ), (UNITY_UINT)(88), UNITY_DISPLAY_STYLE_UINT16);

}







void test_MMA8451Q_F_MODE_REG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(95), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(96), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(97), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(98), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(99), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(100), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(101), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(102), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_F_MODE()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(107), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(108), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(109), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(110), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(111), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(112), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(113), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(114), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_F_STATUS()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(120), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(121), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(122), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(123), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(124), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(125), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(126), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(127), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_F_SETUP()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(132), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(133), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(134), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(135), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(136), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(137), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(138), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(139), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_TRIG_CFG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(144), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(145), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(146), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(147), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_SYSMOD()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(153), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(154), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(155), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(156), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(157), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(158), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(159), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(160), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_INT_SOURCE()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(165), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(166), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(167), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(168), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(169), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(170), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(171), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_XYZ_DATA_CFG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(176), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(177), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(178), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_HP_FILTER_CUTOFF()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(183), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(184), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(185), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(186), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PL_STATUS()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(191), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(192), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(193), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(194), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(195), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PL_CFG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(200), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(201), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PL_COUNT()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(206), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(207), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(208), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(209), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(210), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(211), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(212), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(213), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PL_BF_ZCOMP()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(218), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(219), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(220), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(221), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(222), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PL_THS_REG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(227), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(228), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(229), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(230), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(231), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(232), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(233), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(234), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_FF_MT_CFG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(239), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(240), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(241), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(242), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(243), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_FF_MT_SRC()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(248), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(249), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(250), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(251), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(252), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(253), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(254), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(255), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_FF_MT_THS()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(260), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(261), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(262), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(263), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(264), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(265), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(266), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(267), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_FF_MT_COUNT()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(273), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(274), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(275), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(276), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(277), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(278), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(279), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(280), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_TRANSIENT_CFG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(285), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(286), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(287), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(288), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(289), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_TRANSIENT_SRC()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(294), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(295), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(296), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(297), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(298), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(299), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(300), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(301), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_TRANSIENT_THS()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(306), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(307), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(308), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(309), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(310), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(311), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(312), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(313), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_TRANSIENT_COUNT()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(318), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(319), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(320), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(321), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(322), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(323), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(324), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(325), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PULSE_SRC()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(331), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(332), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(333), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(334), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(335), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(336), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(337), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(338), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PULSE_CFG()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(343), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(344), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(345), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(346), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(347), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(348), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(349), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(350), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PULSE_THSX()

{



    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(356), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(357), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(358), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(359), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(360), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(361), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(362), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PULSE_THSY()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(368), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(369), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(370), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(371), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(372), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(373), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(374), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PULSE_THSZ()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(379), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(380), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(381), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(382), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(383), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(384), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(385), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PULSE_TMLT()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(390), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(391), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(392), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(393), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(394), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(395), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(396), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(397), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_PULSE_LTCY()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(403), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(404), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(405), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(406), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(407), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(408), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(409), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(410), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_PULSE_WIND()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(415), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(416), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(417), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(418), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(419), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(420), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(421), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(422), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_ASLP_COUNT()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(427), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(428), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(429), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(430), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(431), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(432), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(433), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(434), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_CTRL_REG1()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(440), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(441), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(442), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(443), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(444), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(445), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(446), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(447), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_CTRL_REG2()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(453), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(454), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(455), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(456), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(457), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(458), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (UNITY_INT)(UNITY_UINT8 )(((1 << 1))), (

   ((void *)0)

   ), (UNITY_UINT)(459), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(460), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_CTRL_REG3()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(465), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(466), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(467), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(468), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(469), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(470), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(471), UNITY_DISPLAY_STYLE_UINT8);

}





void test_MMA8451Q_CTRL_REG4()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(477), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(478), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(479), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(480), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(481), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(482), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(483), UNITY_DISPLAY_STYLE_UINT8);

}



void test_MMA8451Q_CTRL_REG5()

{

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (UNITY_INT)(UNITY_UINT8 )(((1 << 7))), (

   ((void *)0)

   ), (UNITY_UINT)(488), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (UNITY_INT)(UNITY_UINT8 )(((1 << 6))), (

   ((void *)0)

   ), (UNITY_UINT)(489), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (UNITY_INT)(UNITY_UINT8 )(((1 << 5))), (

   ((void *)0)

   ), (UNITY_UINT)(490), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (UNITY_INT)(UNITY_UINT8 )(((1 << 4))), (

   ((void *)0)

   ), (UNITY_UINT)(491), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (UNITY_INT)(UNITY_UINT8 )(((1 << 3))), (

   ((void *)0)

   ), (UNITY_UINT)(492), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (UNITY_INT)(UNITY_UINT8 )(((1 << 2))), (

   ((void *)0)

   ), (UNITY_UINT)(493), UNITY_DISPLAY_STYLE_UINT8);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (UNITY_INT)(UNITY_UINT8 )(((1 << 0))), (

   ((void *)0)

   ), (UNITY_UINT)(494), UNITY_DISPLAY_STYLE_UINT8);

}
