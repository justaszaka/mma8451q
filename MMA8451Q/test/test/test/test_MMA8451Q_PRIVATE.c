
#include "unity.h"
#include <stdint.h>

#include "stm32f7xx.h"
#include "MMA8451Q.c"
#include "stm32f7xx_hal_dma.h"
#include "stm32f7xx_hal_gpio.h"
#include "mock_stm32f7xx_hal_gpio.h"
#include "mock_stm32f7xx_hal_i2c.h"




/**
 * @brief Test the error list of the MMA8451Q
 * 
 */
void test_MMA8451Q_PRIVATE_Error_Enum_List(void)
{
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,0);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,1);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_WRITE_TO_REG_FAIL,2);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_INCORRECT_ADDRESS,3);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_NO_COMMUNICATION,4);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_TO_REG_FAIL,5);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_ANY_AXIS,6);
}


void test_MMA8451Q_PRIVATE_MMA8451Q_SendData_Incorrect_Register_value(void)
{
    uint8_t DataExample = 0xAA;
    MMA8451Q_t obj;
    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_ERROR);
    MMA8451QWriteRegister(&obj,0x07,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0x08,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0x19,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0x1A,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0x1B,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0x1C,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0x32,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00, obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QWriteRegister(&obj,0xFF,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);
}

void test_MMA8451Q_PRIVATE_MMA8451Q_WriteToRegister_Correct_Register_value(void)
{
    MMA8451Q_t obj;
    uint8_t DataExample = 0xAA;
    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_OK);
    MMA8451QWriteRegister(&obj,REG_STATUS,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK, obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(REG_STATUS, obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(DataExample,obj.tXData);

    MMA8451QWriteRegister(&obj,REG_OUT_Z_LXB ,DataExample);
    TEST_ASSERT_EQUAL_UINT8(obj.errorValue,MMA8451Q_ERROR_OK);
    TEST_ASSERT_EQUAL_UINT8(REG_OUT_Z_LXB,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(DataExample,obj.tXData);

    MMA8451QWriteRegister(&obj,REG_F_SETUP,DataExample);
    TEST_ASSERT_EQUAL_UINT8(obj.errorValue,MMA8451Q_ERROR_OK);
    TEST_ASSERT_EQUAL_UINT8(REG_F_SETUP,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(DataExample,obj.tXData);

    MMA8451QWriteRegister(&obj,REG_FF_MT_COUNT,DataExample);
    TEST_ASSERT_EQUAL_UINT8(obj.errorValue,MMA8451Q_ERROR_OK);
    TEST_ASSERT_EQUAL_UINT8(REG_FF_MT_COUNT,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(DataExample,obj.tXData);

    MMA8451QWriteRegister(&obj,REG_TRANCIENT_CFG,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(REG_TRANCIENT_CFG,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(DataExample,obj.tXData);

    MMA8451QWriteRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(REG_OFF_Z,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(DataExample,obj.tXData);
}

void test_MMA8451Q_PRIVATE_MMA8451Q_WriteToReg_Success(void)
{
    MMA8451Q_t obj;
    uint8_t DataExample = 0xAA;
    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_OK);
    MMA8451QWriteRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(1,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_MMA8451Q_WriteToReg_FAIL(void)
{
    MMA8451Q_t obj;
    uint8_t DataExample = 0xAA;
    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_ERROR);
    MMA8451QWriteRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_WRITE_TO_REG_FAIL,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(HAL_OK,obj.errorValue);

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_BUSY);
    MMA8451QWriteRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_WRITE_TO_REG_FAIL,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(HAL_OK,obj.errorValue);

    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0,HAL_TIMEOUT);
    MMA8451QWriteRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_WRITE_TO_REG_FAIL,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(HAL_OK,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_INIT_INCORRECT_ADDRESS(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;

    MMA8451QInit(&obj,0XFF,&i2cobj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_INCORRECT_ADDRESS,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.devAddress);

    MMA8451QInit(&obj,0xAA,&i2cobj);
    TEST_ASSERT_NOT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(0xAA,obj.devAddress);
}

void test_MMA8451Q_PRIVATE_INIT_CORRECT_ADDRESS(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;
    
    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    obj.rXData = 0x1A;
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA0,&i2cobj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ADDRESS_SA0,obj.devAddress);

    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    obj.rXData = 0x1A;
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA1,&i2cobj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ADDRESS_SA1,obj.devAddress);
}


void test_MMA8451Q_PRIVATE_INIT_INCOCORRECT_I2C_HANDLER(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;
    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA0,&i2cobj);
    TEST_ASSERT_NOT_EQUAL_UINT32(0,obj.i2cHand);
}

void test_MMA8451Q_PRIVATE_INIT_CORRECT_I2C_HANDLER(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;
    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA0,&i2cobj);
    TEST_ASSERT_EQUAL_UINT32(&i2cobj,obj.i2cHand);
}

void test_MMA8451Q_PRIVATE_INIT_FAILED_COMMUNICATION(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;
    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA0,&i2cobj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_NO_COMMUNICATION,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_INIT_FAIL_COMMUNICATION(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;
    obj.rXData = 0xAA;
    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA0,&i2cobj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_NO_COMMUNICATION,obj.errorValue);
}


void test_MMA8451Q_PRIVATE_INIT_SUCCESS_COMMUNICATION(void)
{
    MMA8451Q_t obj;
    I2C_HandleTypeDef i2cobj;
    obj.rXData = 0x1A;
    HAL_I2C_Mem_Read_CMockExpectAnyArgsAndReturn(1, HAL_OK);
    MMA8451QInit(&obj,MMA8451Q_ADDRESS_SA0,&i2cobj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
}


/* MMA8451Q Read data tests */

void test_MMA8451Q_PRIVATE_MMA8451Q_ReadData_Incorrect_Register_value(void)
{
    uint8_t DataExample = 0xAA;
    MMA8451Q_t obj;
    obj.errorValue = HAL_OK ;
    HAL_I2C_Mem_Write_CMockIgnoreAndReturn(0, HAL_ERROR);
    MMA8451QReadRegister(&obj,0x07,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0x08,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0x19,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0x1A,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0x1B,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0x1C,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0x32,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00, obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);

    MMA8451QReadRegister(&obj,0xFF,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_REGISTERS_OUT_OF_RANGE,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.regValue);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.tXData);
}

void test_MMA8451Q_PRIVATE_MMA8451Q_ReadToRegister_Correct_Register_value(void)
{
    MMA8451Q_t obj;
    uint8_t DataExample = 0xAA;
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_OK);
    MMA8451QReadRegister(&obj,REG_STATUS,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK, obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(REG_STATUS, obj.regValue);

    MMA8451QReadRegister(&obj,REG_OUT_Z_LXB ,DataExample);
    TEST_ASSERT_EQUAL_UINT8(obj.errorValue,MMA8451Q_ERROR_OK);
    TEST_ASSERT_EQUAL_UINT8(REG_OUT_Z_LXB,obj.regValue);

    MMA8451QReadRegister(&obj,REG_F_SETUP,DataExample);
    TEST_ASSERT_EQUAL_UINT8(obj.errorValue,MMA8451Q_ERROR_OK);
    TEST_ASSERT_EQUAL_UINT8(REG_F_SETUP,obj.regValue);

    MMA8451QReadRegister(&obj,REG_FF_MT_COUNT,DataExample);
    TEST_ASSERT_EQUAL_UINT8(obj.errorValue,MMA8451Q_ERROR_OK);
    TEST_ASSERT_EQUAL_UINT8(REG_FF_MT_COUNT,obj.regValue);

    MMA8451QReadRegister(&obj,REG_TRANCIENT_CFG,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(REG_TRANCIENT_CFG,obj.regValue);

    MMA8451QReadRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(REG_OFF_Z,obj.regValue);
}

void test_MMA8451Q_PRIVATE_MMA8451Q_ReadFromReg_Success(void)
{
    MMA8451Q_t obj;
    uint8_t DataExample = 0xAA;
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_OK);
    MMA8451QReadRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(1,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_MMA8451Q_ReadFromReg_FAIL(void)
{
    MMA8451Q_t obj;
    uint8_t DataExample = 0xAA;
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_ERROR);
    MMA8451QReadRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_TO_REG_FAIL,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(HAL_OK,obj.errorValue);

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0, HAL_BUSY);
    MMA8451QReadRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_TO_REG_FAIL,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(HAL_OK,obj.errorValue);

    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(0,HAL_TIMEOUT);
    MMA8451QReadRegister(&obj,REG_OFF_Z,DataExample);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_TO_REG_FAIL,obj.errorValue);
    TEST_ASSERT_NOT_EQUAL_UINT8(HAL_OK,obj.errorValue);
}







// void MemtoReadReturnThruPointerI2CPASS(uint8_t ExpectedData)
// {
//     HAL_I2C_Mem_Read_CMockExpectAndReturn(0,0,0,0,0,0,1,0xFFFFFFFF,0);
//     HAL_I2C_Mem_Read_IgnoreArg_hi2c();
//     HAL_I2C_Mem_Read_IgnoreArg_pData();
//     HAL_I2C_Mem_Read_ReturnThruPtr_pData(&ExpectedData);
//     HAL_I2C_Mem_Read_IgnoreArg_DevAddress();
//     HAL_I2C_Mem_Read_IgnoreArg_MemAddress();
//     HAL_I2C_Mem_Read_IgnoreArg_MemAddSize();
// }

// void MemtoReadReturnThruPointerI2CFAIL(uint8_t ExpectedData)
// {
//     HAL_I2C_Mem_Read_CMockExpectAndReturn(0,0,0,0,0,22,1,0xFFFFFFFF,1);
//     HAL_I2C_Mem_Read_IgnoreArg_hi2c();
//     HAL_I2C_Mem_Read_IgnoreArg_pData();
//     HAL_I2C_Mem_Read_ReturnThruPtr_pData(&ExpectedData);
//     HAL_I2C_Mem_Read_IgnoreArg_DevAddress();
//     HAL_I2C_Mem_Read_IgnoreArg_MemAddress();
//     HAL_I2C_Mem_Read_IgnoreArg_MemAddSize();
// }

void test_MMA8451Q_PRIVATE_READ_ALL_AXIS_14BIT_FAIL_COMMONUCATION()
{
    MMA8451Q_t obj;
    uint8_t ExpectedData = 29;
    for (int i = 0; i < 6 ; i++)
    {
        HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_ERROR);
    }
    MMA8451QReadAllAxis14bit(&obj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_NO_COMMUNICATION,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_SUCCESS_COMMUNICATION()
{
    MMA8451Q_t obj = {0};
    uint8_t ExpectedData = 29;
    for (int i = 0; i < 6 ; i++)
    {
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_OK);
    }
    
    MMA8451QReadAllAxis14bit(&obj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_INCORRECT_READING_14_BIT()
{
    MMA8451Q_t obj;
    obj.fastMode = 0;
    obj.xAxisMSB = 0xFF;
    obj.xAxisLSB = 0xFF;
    obj.yAxisMSB = 0x3F;
    obj.yAxisLSB = 0xFF;
    obj.zAxisMSB = 0x3F;
    obj.zAxisLSB = 0xFF;

    for (int i = 0; i < 6 ; i++)
    {
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_OK);
    }
    MMA8451QReadAllAxis14bit(&obj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_ANY_AXIS,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0,obj.xAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.xAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.yAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.yAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.zAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.zAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.xAxisTotal);
    TEST_ASSERT_EQUAL_UINT8(0,obj.zAxisTotal);
    TEST_ASSERT_EQUAL_UINT8(0,obj.yAxisTotal);
}


void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_CORRECT_READING_14_BIT()
{
    MMA8451Q_t obj;
    obj.fastMode = 0;
    obj.xAxisMSB = 0x3A;
    obj.xAxisLSB = 0xFF;
    obj.yAxisMSB = 0x3B;
    obj.yAxisLSB = 0xFF;
    obj.zAxisMSB = 0x3C;
    obj.zAxisLSB = 0xFF;
    for (int i = 0; i < 6 ; i++)
    {
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(456,HAL_OK);
    }
    MMA8451QReadAllAxis14bit(&obj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0x3A,obj.xAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0xFF,obj.xAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0x3B,obj.yAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0xFF,obj.yAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0x3C,obj.zAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0xFF,obj.zAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0x3AFF,obj.xAxisTotal);
    TEST_ASSERT_EQUAL_UINT8(0x3BFF,obj.zAxisTotal);
    TEST_ASSERT_EQUAL_UINT8(0x3CFF,obj.yAxisTotal);
}

void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_INCORRECT_READING_8_BIT()
{
    MMA8451Q_t obj;
    obj.fastMode = 1;
    obj.xAxisMSB = 0xFF;
    obj.xAxisLSB = 1;
    obj.yAxisMSB = 0xFF;
    obj.xAxisLSB = 1;
    obj.zAxisMSB = 0xFF;
    obj.xAxisLSB = 1;


    for (int i = 0; i < 3 ; i++)
    {
        HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_OK);
    }
    MMA8451QReadAllAxis8bit(&obj);
    
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.xAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.xAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.yAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.yAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.zAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.zAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_READ_ANY_AXIS,obj.errorValue);
}

void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_CORRECT_READING_8_BIT()
{
    MMA8451Q_t obj;
    obj.fastMode = 1;
    obj.xAxisMSB = 0xFF;
    obj.xAxisLSB = 0x00;
    obj.yAxisMSB = 0xFF;
    obj.yAxisLSB = 0x00;
    obj.zAxisMSB = 0xFF;
    obj.zAxisLSB = 0x00;
    for (int i = 0; i < 3 ; i++)
    {
    HAL_I2C_Mem_Read_CMockIgnoreAndReturn(456,HAL_OK);
    }
    MMA8451QReadAllAxis8bit(&obj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_OK,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0xFF,obj.xAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.xAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0xFF,obj.yAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.yAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0xFF,obj.zAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0x00,obj.zAxisLSB);
}

void test_MMA8451Q_PRIVATE_READ_DATA_AXIS_FAIL_COMMUNICATING_READING_8_BIT()
{
    MMA8451Q_t obj;
    obj.fastMode = 0;
    obj.xAxisMSB = 0x00;
    obj.xAxisLSB = 0x00;
    obj.yAxisMSB = 0x00;
    obj.yAxisLSB = 0x00;
    obj.zAxisMSB = 0x00;
    obj.zAxisLSB = 0x00;

    for (int i = 0; i < 6 ; i++)
    {
        HAL_I2C_Mem_Read_CMockIgnoreAndReturn(409,HAL_ERROR);
    }
    MMA8451QReadAllAxis8bit(&obj);
    TEST_ASSERT_EQUAL_UINT8(MMA8451Q_ERROR_NO_COMMUNICATION,obj.errorValue);
    TEST_ASSERT_EQUAL_UINT8(0,obj.xAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.xAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.yAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.yAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.zAxisMSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.zAxisLSB);
    TEST_ASSERT_EQUAL_UINT8(0,obj.xAxisTotal);
    TEST_ASSERT_EQUAL_UINT8(0,obj.zAxisTotal);
    TEST_ASSERT_EQUAL_UINT8(0,obj.yAxisTotal);
}
