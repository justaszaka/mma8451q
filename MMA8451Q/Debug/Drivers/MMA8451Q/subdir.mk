################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/MMA8451Q/MMA8451Q.c 

OBJS += \
./Drivers/MMA8451Q/MMA8451Q.o 

C_DEPS += \
./Drivers/MMA8451Q/MMA8451Q.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/MMA8451Q/%.o Drivers/MMA8451Q/%.su: ../Drivers/MMA8451Q/%.c Drivers/MMA8451Q/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F722xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Drivers-2f-MMA8451Q

clean-Drivers-2f-MMA8451Q:
	-$(RM) ./Drivers/MMA8451Q/MMA8451Q.d ./Drivers/MMA8451Q/MMA8451Q.o ./Drivers/MMA8451Q/MMA8451Q.su

.PHONY: clean-Drivers-2f-MMA8451Q

